(ns fest.sem_wrappers
  (:require-macros [cljs.core.async.macros :refer [go alt! go-loop]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as d :include-macros true]))








(defn input [props]
  (d/div #js {:className "ui input"}
    (d/input (clj->js props))))

(defn textarea [props]
  (d/div #js {:className "ui textarea"}
    (d/textarea (clj->js props))))


(defn form-input [label_txt props]
  (if label_txt 
    (d/div #js {:className "ui field input labeled"}
       (d/label nil label_txt)
       (d/input (clj->js props)))
       ;; (d/div #js {:className "ui label"} label_txt))
     (d/div #js {:className "ui field input labeled"}
       (d/input (clj->js props)))))

(defn form-textarea [label_txt props]
  (if label_txt 
    (d/div #js {:className "ui field labeled"}
       ;; (d/div #js {:className "ui label"} label_txt))
       (d/label nil label_txt)
       (d/textarea (clj->js props)))
     (d/div #js {:className "ui field labeled"}
       (d/textarea (clj->js props)))))



(defn icon-button [icon_name txt action]
  ;;(d/div #js {:className "ui vertical labeled icon buttons" 
         (d/div #js {:className "ui small button basic" :onClick action}
                (d/i #js {:className (str "icon " (name icon_name))})
                txt)) ;;)

(defn icon-link [icon_name action]
  (d/i #js {:className (str "ui icon " (name icon_name))
            :onClick action} " "))       



(defn input-change-handler [f]
  (fn [e] 
    (f (.. e -target -value))))

;; TODO simplify the calling structure of all this form stuff
(defn field-input [label_txt curs props]
  (form-input label_txt {:value (get-in curs props)
                         :onBlur (input-change-handler (fn [nv] (om/update! curs props nv)))
                         :onChange (input-change-handler (fn [nv] (om/update! curs props nv)))}))
  
(defn field-textarea [label_txt curs props]
  (form-textarea label_txt {:value (get-in curs props)
                            :onBlur (input-change-handler (fn [nv] (om/update! curs props nv)))
                            :onChange (input-change-handler (fn [nv] (om/update! curs props nv)))}))




;; TODO take extra keys or something
(defn row [r] 
  (d/div #js {:className "row"} r ))

(defn two-column-row [c1 c2]
  (d/div #js {:className "row"} 
    (d/div #js {:className "column"} c1)
    (d/div #js {:className "column"} c2)))
                         
