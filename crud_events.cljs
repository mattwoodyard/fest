(ns fest.crud_events

  (:require-macros [cljs.core.async.macros :refer [go alt!]])
  (:require [om.core :as om :include-macros true]
            [cljs.core.async :refer  [>! put! chan]]
            [fest.client_utils :as cu])

            
  )


(defn strip-map [m  ks]
  (println m ks (get m :path) (into  [] (map #(get m %) ks)))
  (into  [] (map #(get m %) ks)))


;; XXX map #(if (identifier %) newv %) target_vector
;;     so thats waay better
(defn replace-in-vector [target_vector identifier newv]
  (map #(if (identifier %) newv %) target_vector))
  
;;  (let [[begin end] (split-with #(identifier %) target_vector)]
;;    (into [] (apply conj (conj (into [] begin) newv) (rest (into [] end)))))) 


(defn handle-save-update [curs old_id http_response id-fn]
  (println "SAVE U" (:success http_response))
  (if (:success http_response)
    (replace-in-vector curs (fn [obj] (not (= (id-fn obj) old_id))) (:saved (:body http_response)))
   ;; TODO: Here is where we need to rollback the on a 500, for now we could just alert and refresh the page? 
    curs ))  

;; XXX - refactor
;; rpc_name - the base name of the rpc calls
;; schema to send rpc for save
;; 
(defn generic-crud-event-handler [local_app_state_key rpc_name 
                                  rpc_schema extension-fn factory-fn id-fn rpc_map]
  (fn [app [tag-path args :as e]]
;;    (println "GEN CRUD EVT" tag-path)
    (case (first tag-path)
      :add (let [new_model (factory-fn args)]
             (om/transact! (first args) local_app_state_key (fn [curs] (conj curs new_model)))
             (cu/make-rpc-request 
               (or (:new rpc_map) (keyword (str (name rpc_name) "-save")))
               (select-keys new_model rpc_schema)
               (fn [resp]
                 (om/transact! (first args) local_app_state_key 
                               (fn [curs] (handle-save-update curs (id-fn new_model) resp id-fn))))))
             

      :delete (do (om/transact! 
                    (first args) 
                    (fn [curs] (into [] (remove  #(= (id-fn %) (second args)) curs))))

                  (cu/make-rpc-request (or (:delete rpc_map) (keyword (str (name rpc_name) "-delete"))) [(second args)]
                                       (fn [resp]))
                  )


      :modify (let [mod_model (if id-fn (first (filter #(= (id-fn %) (second args)) @(first args)))
                                         @(first args))]
                (cu/make-rpc-request (or (:modify rpc_map) (keyword (str  (name rpc_name) "-save"))) (select-keys mod_model rpc_schema)
                                     (fn [resp] ))

                )

      (if (not (nil? extension-fn)) (extension-fn app e) nil))))

