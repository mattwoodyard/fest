(ns fest.client_utils
  (:require-macros [cljs.core.async.macros :refer [go alt!]])
  (:require [om.core :as om :include-macros true]
            [cljs.core.async :refer  [>! put! chan]]
            [cljs-http.client :as http]
            [goog.events :as events]
            [goog.history.EventType :as EventType])
  (:import goog.History
           goog.History.EventType)
  )


(def query-endpoint "/habitapiv1/query/")
(def rpc-endpoint "/habitapiv1/rpc/")

(defn make-rpc-query [obj_type params rf]
  (go (rf (<! (http/get (str query-endpoint (name obj_type))
             {:query-params params})))))



(defn make-rpc-request [fn-name args rf]
  (go (rf (<! (http/post rpc-endpoint 
             {:json-params {:fn_name fn-name :args args}})))))




