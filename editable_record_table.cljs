(ns fest.editable_record_table
  (:require-macros [cljs.core.async.macros :refer [go alt!]])
  (:require [om.core :as om :include-macros true]
            [cljs.core.async :refer  [>! put! chan]]
            [om.dom :as d :include-macros true]
            [cljs_tmpl.kv-tags :as kvt]

            [fest.sem_wrappers :as s]
            [goog.events :as events]
            [goog.history.EventType :as EventType])
  (:import goog.History
           goog.ui.IdGenerator
           goog.History.EventType)
  )

(defn guid []
    (.getNextUniqueId (.getInstance goog.ui.IdGenerator)))


(defn header-row [columns edit_disabled]
  (d/thead nil (apply d/tr nil 
                      (d/th nil "")
;;                      (when (not edit_disabled) (d/th nil "E"))
                      (map #(d/th nil (:header_txt %)) columns))))


(defn input-change-handler [f]
  (fn [e] 
    (f (.. e -target -value))))



(defn render-editting-cursor-attr [curs attr ch]
  (ch (get-in curs attr) true #(om/update! curs attr %) #(om/update! curs attr %) nil))

(defn attribute-render [attr-fn ch] (fn [ & args] 
  (apply ch (attr-fn (first args)) (rest args))))

(defn raw-render [txt owner isetter setter link label] 
  txt)


(defn raw-render-textbox [txt editting isetter setter link]
  (if (not editting)
    txt 
    (d/textarea #js {:value txt
                  :onBlur (input-change-handler isetter)
                  :onChange (input-change-handler isetter)})))

(defn raw-render-input [txt editting isetter setter link]
  (if (not editting)
    txt 
    (s/input {:value txt
              :onBlur (input-change-handler isetter)
              :onChange (input-change-handler isetter)})))

(defn raw-render-link-input [txt editting isetter setter link]
  (if (not editting)
    (if link
      (d/a #js {:href link} txt)
      txt )
    (s/input {:value txt
              :onBlur (input-change-handler isetter)
              :onChange (input-change-handler isetter)})))




(defn kvtag-render [dict editting isetter setter link]
  (om/build kvt/tag-editor dict {:opts {:onChange isetter}}))



(defn evt-build [pfx tg]
  (into [] (conj pfx tg)))

(defn editable-record-table-row [row owner {:keys [columns collection event-pfx actions                     
                                                   edit_disabled event-channel link-builder]}]
  (reify
    ;; XXX - initial page state
    om/IInitState
    (init-state [_] {:editting false
                     :begin 0
                     :end 0
                     :page-width 10})

    om/IRenderState
    (render-state [_ s]
      (apply d/tr nil
             (apply d/td nil
                   (map (fn [a]
                          (if (:show-when-fn a)
                            (d/span #js
                                    {:onClick ((:action-fn a) owner event-channel event-pfx collection @row)}
                                    (:display a))
                                     
                            nil)) actions))




             ;; XXX how to override the column def

             (map (fn [c]
                    (let [fld_val (if (vector? c) (get-in row c) (get row c)) ;; (c row) 
                          renderf (get c :renderer raw-render-input)
                          ;;(get column_renderers c raw-render-input)
                          ;; isetter (fn [nv] (om/set-state owner c nv))
                          setter (fn [nv] (om/update! row (:field_selector c) nv))
                          isetter (fn [nv] (om/update! row (:field_selector c) nv))
                          link (if link-builder (link-builder row) nil) 
                          ]
                      ;; XXX - I'm not sure that we won't want to have a bunch of other
                      ;; information about the thing?
                      (d/td nil (renderf fld_val (om/get-state owner :editting) isetter setter link))
                      )
                    ) columns )) 
      )))



(defrecord ColumnDef [header_txt field_selector renderer editable])
(defrecord RowAction [show-when-fn display action-fn])



(defn when-editting [row owner] (om/get-state owner :editting))
(defn icon-display [icon_name] (d/i #js {:className (str "ui icon " (name icon_name))}))



(defn row-remove-yield [owner event-channel event-pfx collection row]
  (put! event-channel [(evt-build event-pfx :delete) [collection (:id @row)]]))

(defn row-save-yield  [owner event-channel event-pfx collection row] )
(defn row-undo-yield  [owner event-channel event-pfx collection row] )

(def remove-row-action (RowAction. when-editting (icon-display :remove) row-remove-yield))
(def save-row-action (RowAction. when-editting (icon-display :save) row-save-yield))
(def cancel-edit-row-action (RowAction. when-editting (icon-display :undo) row-undo-yield))


(defn column-def [header_txt field_selector renderer editable]
  (ColumnDef. header_txt field_selector renderer editable))


(defn editable-record-table [rows owner {:keys [columns footer-fn edit_disabled
                                                event-channel event-pfx link-builder]}]
  (reify 
    om/IRender
    (render [_]
      (d/table  #js {:className "table ui"} 
                (header-row columns edit_disabled)
                (apply d/tbody nil
                       (conj (om/build-all editable-record-table-row 
                                           rows {:opts {:event-channel event-channel
                                                        :columns columns
                                                        :link-builder link-builder
                                                        :edit_disabled edit_disabled
                                                        :event-pfx event-pfx
                                                        :collection rows}})
                             ;;))))))
                             (if footer-fn (footer-fn rows) nil)))))))

