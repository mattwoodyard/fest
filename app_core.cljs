(ns fest.app_core
  (:require-macros [cljs.core.async.macros :refer [go alt! go-loop]])
  (:require [om.core :as om :include-macros true]
            [cljs.core.async :refer  [>! put! chan timeout close!]]


            [goog.events :as events]
            [goog.history.EventType :as EventType])
  (:import goog.History
           goog.History.EventType)
  )



(defn app-runner [app owner {:keys [handle-events]}]
  (reify 
    om/IWillMount
    (will-mount [_]
      (let [comm (chan)]
        (om/set-state! owner [:event-channel] comm)
        (go-loop [cur_evt (<! comm)]
            (if cur_evt 
              (do 
                (handle-events app cur_evt)
                (recur (<! comm))
                )
              nil))))
    om/IWillUnmount
    (will-unmount [_]
      (close! (om/get-state owner [:event-channel])))
    om/IRender
    (render [_]
      ;;(if (get-in app [:nav-state :view])
        (om/build (get-in app [:nav-state :view]) app
                  {:opts {:event-channel (om/get-state owner :event-channel)}}))
))




