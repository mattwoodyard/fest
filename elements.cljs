(ns fest.elements)







  
(defn add-class [el classname] 
  (let [[tag attr body] el]
    [tag (assoc attr :class-name  (conj  (get attr :class-name []) classname)) body]))


(defn ui [el] (add-class el "ui"))
(defn field-inline [el] (add-class el "field inline"))
(defn row [el] (add-class el "row"))
(defn cwide [el wide] (add-class el (str "column " wide)))

  



(defn input [])

(defn tag [t] [t {} []])
(defn select [attrs & body] [:select attrs body])

(defn set-attr [el k v]
  (let [[tag attr body] el]
    [tag (assoc attr k v) body]))


(defn change-handler [el f] (set-attr el :on-change f))


(defn body [el & b]
  (let [[tag attr body] el]
    [tag attr (apply conj body b)]))

(defn body-list [el b]
  (let [[tag attr body] el]
    [tag attr (concat body b)]))



(defn icon [nm]
  (-> [:i {} nil]
      (set-attr :class-name ["icon" (name nm)])))

(defn on-click [el f] (set-attr el :on-click f))



(defn icon-button [icn txt]
  (-> [:div {} []]
      (body-list [(icon icn) txt])
      (ui)
      (add-class "button")
      (add-class "primary")
      (add-class "small")
      ))

