(ns fest.rpc_ac_widget
  (:require-macros [cljs.core.async.macros :refer [go alt!]])
  (:require [om.core :as om :include-macros true]
            [cljs.core.async :refer  [>! put! chan timeout]]
            [cljs-http.client :as http]
            [om.dom :as d :include-macros true]
            [secretary.core :as secretary :include-macros true :refer [defroute]]
            [fest.client_utils :as cutil]
            [arosequist.om-autocomplete :as ac]
            [arosequist.om-autocomplete.semantic :as ac-bootstrap]
            [goog.events :as events]
            [goog.history.EventType :as EventType])
  (:import goog.History
           goog.History.EventType)
  )


(defn loading []
  (reify
    om/IRender
    (render [_]
      (d/span nil " Loading..."))))

(defn build-suggested [qry qryp]
(fn suggestions [value suggestions-ch _]
  (cutil/make-rpc-query 
     qry {qryp value} 
     (fn [resp]
        (put! suggestions-ch (:body resp))
       ))))




(defn autocomplete [app owner {:keys [:on-change :ilabel :query-name :query-param :class-name]}]
  (reify
    om/IInitState
     (init-state [_]
       {:result-ch (chan)})

    om/IWillMount
    (will-mount [_]
      (let [result-ch (om/get-state owner :result-ch)]
        (go (loop []
          (let [[idx result] (<! result-ch)]
            (on-change result)
            (recur))))))

    om/IRenderState
    (render-state [_ {:keys [result-ch value]}]

      (om/build ac/autocomplete app
                {:opts
                 {:container-view ac-bootstrap/container-view
                  :container-view-opts {:class-name class-name}
                  :input-view ac-bootstrap/input-view
                  :input-view-opts {:placeholder ilabel :class-name class-name}
                  :results-view ac-bootstrap/results-view
                  :results-view-opts {:loading-view loading
                                      :render-item ac-bootstrap/render-item
                                      :render-item-opts {:text-fn (fn [item _] (:name item))}}
                  :result-ch result-ch
                  :suggestions-fn (build-suggested query-name query-param)}
                 :state {:value value}
                 }))))



